﻿var limit = 0;

function deleteimage() {
  var ximg = document.getElementById("pic" + (limit - 1));
  document.getElementById("pecs").removeChild(ximg);
  limit--;
}

function showimage(id, category) {
  var fileExt = '.png';
  if (limit < 50) {
    var imgPath = "pecs/";
    if (!category)
      imgPath += 'categories/' + id  + fileExt;
    else if (category === 'localstorage')
      imgPath = id;
    else
      imgPath += (category + '/' + id + fileExt);
    document.getElementById('pecs').innerHTML += "<img src='" + imgPath +"' height='44%' width='14%' id='pic" + limit + "'  border='0' />";
  limit++;
  }
}

function opendiv(id) {
  document.getElementById("categories0").style.display = "none";
  document.getElementById("categories1").style.display = "none";
  document.getElementById("categories2").style.display = "none";
  document.getElementById("categories3").style.display = "none";
  document.getElementById("categories4").style.display = "none";
  document.getElementById("categories5").style.display = "none";
  document.getElementById("categories6").style.display = "none";
  document.getElementById("categories7").style.display = "none";
  document.getElementById("categories8").style.display = "none";
  document.getElementById("categories9").style.display = "none";
  document.getElementById("categories10").style.display = "none";
  document.getElementById("alphabet0").style.display = "none";
  document.getElementById("alphabet1").style.display = "none";
  document.getElementById("alphabet2").style.display = "none";
  document.getElementById("alphabet3").style.display = "none";
  document.getElementById("alphabet4").style.display = "none";
  document.getElementById("alphabet5").style.display = "none";
  document.getElementById("animals0").style.display = "none";
  document.getElementById("animals1").style.display = "none";
  document.getElementById("animals2").style.display = "none";
  document.getElementById("animals3").style.display = "none";
  document.getElementById("bathroom0").style.display = "none";
  document.getElementById("bathroom1").style.display = "none";
  document.getElementById("behaviours0").style.display = "none";
  document.getElementById("behaviours1").style.display = "none";
  document.getElementById("bodyparts0").style.display = "none";
  document.getElementById("bodyparts1").style.display = "none";
  document.getElementById("clothing0").style.display = "none";
  document.getElementById("clothing1").style.display = "none";
  document.getElementById("colors0").style.display = "none";
  document.getElementById("colors1").style.display = "none";
  document.getElementById("days0").style.display = "none";
  document.getElementById("desserts0").style.display = "none";
  document.getElementById("desserts1").style.display = "none";
  document.getElementById("dressing0").style.display = "none";
  document.getElementById("entertainment0").style.display = "none";
  document.getElementById("entertainment1").style.display = "none";
  document.getElementById("foods0").style.display = "none";
  document.getElementById("foods1").style.display = "none";
  document.getElementById("foods2").style.display = "none";
  document.getElementById("foods3").style.display = "none";
  document.getElementById("fruits0").style.display = "none";
  document.getElementById("fruits1").style.display = "none";
  document.getElementById("actions0").style.display = "none";
  document.getElementById("actions1").style.display = "none";
  document.getElementById("help0").style.display = "none";
  document.getElementById("help1").style.display = "none";
  document.getElementById("group0").style.display = "none";
  document.getElementById("holidays0").style.display = "none";
  document.getElementById("individuals0").style.display = "none";
  document.getElementById("kitchen0").style.display = "none";
  document.getElementById("kitchen1").style.display = "none";
  document.getElementById("months0").style.display = "none";
  document.getElementById("months1").style.display = "none";
  document.getElementById("moods0").style.display = "none";
  document.getElementById("music0").style.display = "none";
  document.getElementById("pain0").style.display = "none";
  document.getElementById("personalcare0").style.display = "none";
  document.getElementById("places0").style.display = "none";
  document.getElementById("places1").style.display = "none";
  document.getElementById("places2").style.display = "none";
  document.getElementById("places3").style.display = "none";
  document.getElementById("play0").style.display = "none";
  document.getElementById("senses0").style.display = "none";
  document.getElementById("shapes0").style.display = "none";
  document.getElementById("sports0").style.display = "none";
  document.getElementById("time0").style.display = "none";
  document.getElementById("time1").style.display = "none";
  document.getElementById("tools0").style.display = "none";
  document.getElementById("transportation0").style.display = "none";
  document.getElementById("vegetables0").style.display = "none";
  document.getElementById("vegetables1").style.display = "none";
  document.getElementById("weather0").style.display = "none";
  document.getElementById("family0").style.display = "none";
  document.getElementById("comments0").style.display = "none";
  document.getElementById("comments1").style.display = "none";
  document.getElementById("manners0").style.display = "none";
  document.getElementById("hibye0").style.display = "none";
  document.getElementById("hibye1").style.display = "none";
  document.getElementById("yesnomaybe0").style.display = "none";
  document.getElementById("questions0").style.display = "none";
  document.getElementById("questions1").style.display = "none";
  document.getElementById("myfiles0").style.display = "none";
  document.getElementById("numbers0").style.display = "none";
  document.getElementById("numbers1").style.display = "none";
  document.getElementById("corewords0").style.display = "none";
  document.getElementById("corewords1").style.display = "none";
  document.getElementById("corewords2").style.display = "none";
  document.getElementById("corewords3").style.display = "none";
  document.getElementById("corewords4").style.display = "none";
  switch (id) {
    case 'cat0':
      document.getElementById("categories0").style.display = "block";
      break;
    case 'cat1':
      document.getElementById("categories1").style.display = "block";
      break;
    case 'cat2':
      document.getElementById("categories2").style.display = "block";
      break;
    case 'cat3':
      document.getElementById("categories3").style.display = "block";
      break;
    case 'cat4':
      document.getElementById("categories4").style.display = "block";
      break;
    case 'cat5':
      document.getElementById("categories5").style.display = "block";
      break;
    case 'cat6':
      document.getElementById("categories6").style.display = "block";
      break;
    case 'cat7':
      document.getElementById("categories7").style.display = "block";
      break;
    case 'cat8':
      document.getElementById("categories8").style.display = "block";
      break;
    case 'cat9':
      document.getElementById("categories9").style.display = "block";
      break;
    case 'cat10':
      document.getElementById("categories10").style.display = "block";
      break;
    case 'alp0':
      document.getElementById("alphabet0").style.display = "block";
      break;
    case 'alp1':
      document.getElementById("alphabet1").style.display = "block";
      break;
    case 'alp2':
      document.getElementById("alphabet2").style.display = "block";
      break;
    case 'alp3':
      document.getElementById("alphabet3").style.display = "block";
      break;
    case 'alp4':
      document.getElementById("alphabet4").style.display = "block";
      break;
    case 'alp5':
      document.getElementById("alphabet5").style.display = "block";
      break;
    case 'ani0':
      document.getElementById("animals0").style.display = "block";
      break;
    case 'ani1':
      document.getElementById("animals1").style.display = "block";
      break;
    case 'ani2':
      document.getElementById("animals2").style.display = "block";
      break;
    case 'ani3':
      document.getElementById("animals3").style.display = "block";
      break;
    case 'bath0':
      document.getElementById("bathroom0").style.display = "block";
      break;
    case 'bath1':
      document.getElementById("bathroom1").style.display = "block";
      break;
    case 'beh0':
      document.getElementById("behaviours0").style.display = "block";
      break;
    case 'beh1':
      document.getElementById("behaviours1").style.display = "block";
      break;
    case 'bod0':
      document.getElementById("bodyparts0").style.display = "block";
      break;
    case 'bod1':
      document.getElementById("bodyparts1").style.display = "block";
      break;
    case 'clo0':
      document.getElementById("clothing0").style.display = "block";
      break;
    case 'clo1':
      document.getElementById("clothing1").style.display = "block";
      break;
    case 'col0':
      document.getElementById("colors0").style.display = "block";
      break;
    case 'col1':
      document.getElementById("colors1").style.display = "block";
      break;
    case 'day0':
      document.getElementById("days0").style.display = "block";
      break;
    case 'dess0':
      document.getElementById("desserts0").style.display = "block";
      break;
    case 'dess1':
      document.getElementById("desserts1").style.display = "block";
      break;
    case 'dre0':
      document.getElementById("dressing0").style.display = "block";
      break;
    case 'ent0':
      document.getElementById("entertainment0").style.display = "block";
      break;
    case 'ent1':
      document.getElementById("entertainment1").style.display = "block";
      break;
    case 'foo0':
      document.getElementById("foods0").style.display = "block";
      break;
    case 'foo1':
      document.getElementById("foods1").style.display = "block";
      break;
    case 'foo2':
      document.getElementById("foods2").style.display = "block";
      break;
    case 'foo3':
      document.getElementById("foods3").style.display = "block";
      break;
    case 'fru0':
      document.getElementById("fruits0").style.display = "block";
      break;
    case 'fru1':
      document.getElementById("fruits1").style.display = "block";
      break;
    case 'act0':
      document.getElementById("actions0").style.display = "block";
      break;
    case 'act1':
      document.getElementById("actions1").style.display = "block";
      break;
    case 'hel0':
      document.getElementById("help0").style.display = "block";
      break;
    case 'hel1':
      document.getElementById("help1").style.display = "block";
      break;
    case 'gro0':
      document.getElementById("group0").style.display = "block";
      break;
    case 'hol0':
      document.getElementById("holidays0").style.display = "block";
      break;
    case 'ind0':
      document.getElementById("individuals0").style.display = "block";
      break;
    case 'kit0':
      document.getElementById("kitchen0").style.display = "block";
      break;
    case 'kit1':
      document.getElementById("kitchen1").style.display = "block";
      break;
    case 'mon0':
      document.getElementById("months0").style.display = "block";
      break;
    case 'mon1':
      document.getElementById("months1").style.display = "block";
      break;
    case 'moo0':
      document.getElementById("moods0").style.display = "block";
      break;
    case 'mus0':
      document.getElementById("music0").style.display = "block";
      break;
    case 'pai0':
      document.getElementById("pain0").style.display = "block";
      break;
    case 'per0':
      document.getElementById("personalcare0").style.display = "block";
      break;
    case 'pla0':
      document.getElementById("places0").style.display = "block";
      break;
    case 'pla1':
      document.getElementById("places1").style.display = "block";
      break;
    case 'pla2':
      document.getElementById("places2").style.display = "block";
      break;
    case 'pla3':
      document.getElementById("places3").style.display = "block";
      break;
    case 'ply0':
      document.getElementById("play0").style.display = "block";
      break;
    case 'sen0':
      document.getElementById("senses0").style.display = "block";
      break;
    case 'sha0':
      document.getElementById("shapes0").style.display = "block";
      break;
    case 'spo0':
      document.getElementById("sports0").style.display = "block";
      break;
    case 'tim0':
      document.getElementById("time0").style.display = "block";
      break;
    case 'tim1':
      document.getElementById("time1").style.display = "block";
      break;
    case 'too0':
      document.getElementById("tools0").style.display = "block";
      break;
    case 'tra0':
      document.getElementById("transportation0").style.display = "block";
      break;
    case 'veg0':
      document.getElementById("vegetables0").style.display = "block";
      break;
    case 'veg1':
      document.getElementById("vegetables1").style.display = "block";
      break;
    case 'wea0':
      document.getElementById("weather0").style.display = "block";
      break;
    case 'fam0':
      document.getElementById("family0").style.display = "block";
      break;
    case 'com0':
      document.getElementById("comments0").style.display = "block";
      break;
    case 'com1':
      document.getElementById("comments1").style.display = "block";
      break;
    case 'man0':
      document.getElementById("manners0").style.display = "block";
      break;
    case 'hib0':
      document.getElementById("hibye0").style.display = "block";
      break;
    case 'hib1':
      document.getElementById("hibye1").style.display = "block";
      break;
    case 'yes0':
      document.getElementById("yesnomaybe0").style.display = "block";
      break;
    case 'que0':
      document.getElementById("questions0").style.display = "block";
      break;
    case 'que1':
      document.getElementById("questions1").style.display = "block";
      break;
    case 'num0':
      document.getElementById("numbers0").style.display = "block";
      break;
    case 'num1':
      document.getElementById("numbers1").style.display = "block";
      break;
    case 'cor0':
      document.getElementById("corewords0").style.display = "block";
      break;
    case 'cor1':
      document.getElementById("corewords1").style.display = "block";
      break;
    case 'cor2':
      document.getElementById("corewords2").style.display = "block";
      break;
    case 'cor3':
      document.getElementById("corewords3").style.display = "block";
      break;
    case 'cor4':
      document.getElementById("corewords4").style.display = "block";
      break;
    case 'myf0':
      if (localStorage["currentid"]) {
        var x = localStorage.getItem("currentid");
        /*for (i = 0; i <= x; i++) {
          console.log('first', 'imgData' + i)
          dataImage = localStorage.getItem('imgData' + i);
          var bannerContainer = document.createElement('div');
          bannerContainer.className = 'banner-container'
          var bannerImg = document.createElement("img");
          bannerImg.className = 'uploaded-image'
          bannerImg.src = dataImage;

          var deleteBtn = document.createElement('button');
          deleteBtn.type = "button";
          deleteBtn.className = "rm-pic";

          (function (index, btn, container){
            btn.addEventListener('click', function (e) {
              console.log('imgData' + index);
              localStorage.removeItem(('imgData' + index));
              var currentId = localStorage.getItem('currentid');
              currentId = parseInt(currentId);
              currentId = currentId > 0 ? --currentId : currentId;
              localStorage.setItem('currentid', currentId.toString());
              container.parentElement.removeChild(container);
            });
          }(i, deleteBtn, bannerContainer));

          bannerContainer.appendChild(deleteBtn);
          bannerContainer.appendChild(bannerImg);
          document.getElementById("bannerDiv").appendChild(bannerContainer);

        }*/

        for (var key in localStorage) {
          if (localStorage.hasOwnProperty(key)) {
            if (key.indexOf('imgData') != -1) {
              // this is an images
              dataImage = localStorage.getItem(key);
              var bannerContainer = document.createElement('div');
              bannerContainer.className = 'banner-container'
              var bannerImg = document.createElement("img");
              bannerImg.className = 'uploaded-image'
              bannerImg.src = dataImage;

              var deleteBtn = document.createElement('button');
              deleteBtn.type = "button";
              deleteBtn.className = "rm-pic";

              (function (key, btn, container){
                btn.addEventListener('click', function (e) {
                  console.log(key);
                  localStorage.removeItem(key);
                  var currentId = localStorage.getItem('currentid');
                  currentId = parseInt(currentId);
                  currentId = currentId > 0 ? --currentId : currentId;
                  localStorage.setItem('currentid', currentId.toString());
                  container.parentElement.removeChild(container);
                });
              }(key, deleteBtn, bannerContainer));

              bannerContainer.appendChild(deleteBtn);
              bannerContainer.appendChild(bannerImg);
              document.getElementById("bannerDiv").appendChild(bannerContainer);

            }
          }
        }
      }
      document.getElementById("myfiles0").style.display = "block";
      var uploadedImages = document.querySelectorAll('.uploaded-image');
      uploadedImages = Array.prototype.slice.call(uploadedImages);
      uploadedImages.forEach(function(element) {
        element.addEventListener('click', function (e) {
          showimage(e.target.src, 'localstorage');
        });
      }.bind(this));
      break;
  }
}

    var id = 100;
    function openfile(e) {
      var container = document.getElementById('bannerDiv');
      var img = document.createElement('img');
      img.className = "uploaded-image";
      // set styles

      //document.getElementById('bannerImg').style.display = "block";

      if (e.files && e.files[0]) {
        var reader = new FileReader();

        var x = 0;
        var current = 0;
        reader.onload = function (ee) {
          //document.getElementById('bannerImg').src = ee.target.result;
          img.src = ee.target.result;
          //bannerImage = document.getElementById('bannerImg');
          imgData = ee.target.result;
          if (localStorage["currentid"]) {
            x = parseInt(localStorage["currentid"]);
            ++x;
          }
          localStorage["currentid"] = x;
          window.imgData = imgData;
          localStorage.setItem("imgData" + x, imgData);
          console.log('ADDING NEW','imgData' + x);

          var bannerContainer = document.createElement('div');
          bannerContainer.className = 'banner-container'

          var deleteBtn = document.createElement('button');
          deleteBtn.type = "button";
          deleteBtn.className = "rm-pic";

          (function (index, btn, container){
            btn.addEventListener('click', function (e) {
              console.log(index);
              localStorage.removeItem(('imgData' + index));
              var currentId = localStorage.getItem('currentid');
              currentId = parseInt(currentId);
              currentId = currentId > 0 ? --currentId : currentId;
              localStorage.setItem('currentid', currentId.toString());
              container.parentElement.removeChild(container);
            });
          }(x, deleteBtn, bannerContainer));

          bannerContainer.appendChild(deleteBtn);
          bannerContainer.appendChild(img);
          container.appendChild(bannerContainer);

          img.addEventListener('click', function (e) {
            showimage(e.target.src, 'localstorage');
          });

        }
        reader.readAsDataURL(e.files[0]);
      }
    }
