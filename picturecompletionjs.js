﻿
var playAudio = function () {
  var audio = new Audio('/audio/gj.mp3');
  audio.play();
}

function opendiv(id) {
    document.getElementById("instructions0").style.display = "none";
    document.getElementById("gamecategories0").style.display = "none";
    document.getElementById("progressreports0").style.display = "none";
    document.getElementById("numberslevel1").style.display = "none";
    document.getElementById("keepgoing").style.display = "none";
    document.getElementById("goldmedal").style.display = "none";
    document.getElementById("alphabetlevel1").style.display = "none";
    document.getElementById("animalslevel1").style.display = "none";
    document.getElementById("colorslevel1").style.display = "none";
    document.getElementById("foodslevel1").style.display = "none";
    switch (id) {
        case 'ins0':
            document.getElementById("instructions0").style.display = "block";
            break;
        case 'gamecat0':
            document.getElementById("gamecategories0").style.display = "block";
            document.getElementById("numberslevel1show").style.display = "none";
            document.getElementById("numberslevel1choices").style.display = "none";
            document.getElementById("numberslevel2show").style.display = "none";
            document.getElementById("numberslevel2choices").style.display = "none";
            document.getElementById("numberslevel3show").style.display = "none";
            document.getElementById("numberslevel3choices").style.display = "none";
            document.getElementById("numberslevel4show").style.display = "none";
            document.getElementById("numberslevel4choices").style.display = "none";
            document.getElementById("numberslevel5show").style.display = "none";
            document.getElementById("numberslevel5choices").style.display = "none";
            document.getElementById("alphabetlevel1show").style.display = "none";
            document.getElementById("alphabetlevel1choices").style.display = "none";
            document.getElementById("alphabetlevel2show").style.display = "none";
            document.getElementById("alphabetlevel2choices").style.display = "none";
            document.getElementById("alphabetlevel3show").style.display = "none";
            document.getElementById("alphabetlevel3choices").style.display = "none";
            document.getElementById("alphabetlevel4show").style.display = "none";
            document.getElementById("alphabetlevel4choices").style.display = "none";
            document.getElementById("alphabetlevel5show").style.display = "none";
            document.getElementById("alphabetlevel5choices").style.display = "none";
            document.getElementById("animalslevel1show").style.display = "none";
            document.getElementById("animalslevel1choices").style.display = "none";
            document.getElementById("animalslevel2show").style.display = "none";
            document.getElementById("animalslevel2choices").style.display = "none";
            document.getElementById("animalslevel3show").style.display = "none";
            document.getElementById("animalslevel3choices").style.display = "none";
            document.getElementById("animalslevel4show").style.display = "none";
            document.getElementById("animalslevel4choices").style.display = "none";
            document.getElementById("animalslevel5show").style.display = "none";
            document.getElementById("animalslevel5choices").style.display = "none";
            document.getElementById("colorslevel1show").style.display = "none";
            document.getElementById("colorslevel1choices").style.display = "none";
            document.getElementById("colorslevel2show").style.display = "none";
            document.getElementById("colorslevel2choices").style.display = "none";
            document.getElementById("colorslevel3show").style.display = "none";
            document.getElementById("colorslevel3choices").style.display = "none";
            document.getElementById("colorslevel4show").style.display = "none";
            document.getElementById("colorslevel4choices").style.display = "none";
            document.getElementById("colorslevel5show").style.display = "none";
            document.getElementById("colorslevel5choices").style.display = "none";
            document.getElementById("foodslevel1show").style.display = "none";
            document.getElementById("foodslevel1choices").style.display = "none";
            document.getElementById("foodslevel2show").style.display = "none";
            document.getElementById("foodslevel2choices").style.display = "none";
            document.getElementById("foodslevel3show").style.display = "none";
            document.getElementById("foodslevel3choices").style.display = "none";
            document.getElementById("foodslevel4show").style.display = "none";
            document.getElementById("foodslevel4choices").style.display = "none";
            document.getElementById("foodslevel5show").style.display = "none";
            document.getElementById("foodslevel5choices").style.display = "none";
            document.getElementById("congrats").style.display = "none";
            break;
        case 'progrep0':
            document.getElementById("progressreports0").style.display = "block";
            break;
        case 'numlev1':
            timer = setTimeout(fadetoblack, 3000);
            document.getElementById("numberslevel1").style.display = "block";
            break;
        case 'alplev1':
            timer = setTimeout(fadetoblack, 3000);
            document.getElementById("alphabetlevel1").style.display = "block";
            break;
        case 'anilev1':
            timer = setTimeout(fadetoblack, 3000);
            document.getElementById("animalslevel1").style.display = "block";
            break;
        case 'collev1':
            timer = setTimeout(fadetoblack, 3000);
            document.getElementById("colorslevel1").style.display = "block";
            break;
        case 'foolev1':
            timer = setTimeout(fadetoblack, 3000);
            document.getElementById("foodslevel1").style.display = "block";
            break;
        case 'kg':
            if (document.getElementById("numberslevel1show").style.display == "block") {
                timer = setTimeout(numberslevel1question, 3000);
                document.getElementById("keepgoing").style.display = "block";
                document.getElementById("numberslevel1show").style.display = "none";
                document.getElementById("numberslevel1choices").style.display = "none";
            }
            else if (document.getElementById("numberslevel2show").style.display == "block") {
                timer = setTimeout(numberslevel2question, 3000);
                document.getElementById("keepgoing").style.display = "block";
                document.getElementById("numberslevel2show").style.display = "none";
                document.getElementById("numberslevel2choices").style.display = "none";
            }
            else if (document.getElementById("numberslevel3show").style.display == "block") {
                timer = setTimeout(numberslevel3question, 3000);
                document.getElementById("keepgoing").style.display = "block";
                document.getElementById("numberslevel3show").style.display = "none";
                document.getElementById("numberslevel3choices").style.display = "none";
            }
            else if (document.getElementById("numberslevel4show").style.display == "block") {
                timer = setTimeout(numberslevel4question, 3000);
                document.getElementById("keepgoing").style.display = "block";
                document.getElementById("numberslevel4show").style.display = "none";
                document.getElementById("numberslevel4choices").style.display = "none";
            }
            else if (document.getElementById("numberslevel5show").style.display == "block") {
                timer = setTimeout(numberslevel5question, 3000);
                document.getElementById("keepgoing").style.display = "block";
                document.getElementById("numberslevel5show").style.display = "none";
                document.getElementById("numberslevel5choices").style.display = "none";
            }
            else if (document.getElementById("alphabetlevel1show").style.display == "block") {
                timer = setTimeout(alphabetlevel1question, 3000);
                document.getElementById("keepgoing").style.display = "block";
                document.getElementById("alphabetlevel1show").style.display = "none";
                document.getElementById("alphabetlevel1choices").style.display = "none";
            }
            else if (document.getElementById("alphabetlevel2show").style.display == "block") {
                timer = setTimeout(alphabetlevel2question, 3000);
                document.getElementById("keepgoing").style.display = "block";
                document.getElementById("alphabetlevel2show").style.display = "none";
                document.getElementById("alphabetlevel2choices").style.display = "none";
            }
            else if (document.getElementById("alphabetlevel3show").style.display == "block") {
                timer = setTimeout(alphabetlevel3question, 3000);
                document.getElementById("keepgoing").style.display = "block";
                document.getElementById("alphabetlevel3show").style.display = "none";
                document.getElementById("alphabetlevel3choices").style.display = "none";
            }
            else if (document.getElementById("alphabetlevel4show").style.display == "block") {
                timer = setTimeout(alphabetlevel4question, 3000);
                document.getElementById("keepgoing").style.display = "block";
                document.getElementById("alphabetlevel4show").style.display = "none";
                document.getElementById("alphabetlevel4choices").style.display = "none";
            }
            else if (document.getElementById("alphabetlevel5show").style.display == "block") {
                timer = setTimeout(alphabetlevel5question, 3000);
                document.getElementById("keepgoing").style.display = "block";
                document.getElementById("alphabetlevel5show").style.display = "none";
                document.getElementById("alphabetlevel5choices").style.display = "none";
            }
            else if (document.getElementById("animalslevel1show").style.display == "block") {
                timer = setTimeout(animalslevel1question, 3000);
                document.getElementById("keepgoing").style.display = "block";
                document.getElementById("animalslevel1show").style.display = "none";
                document.getElementById("animalslevel1choices").style.display = "none";
            }
            else if (document.getElementById("animalslevel2show").style.display == "block") {
                timer = setTimeout(animalslevel2question, 3000);
                document.getElementById("keepgoing").style.display = "block";
                document.getElementById("animalslevel2show").style.display = "none";
                document.getElementById("animalslevel2choices").style.display = "none";
            }
            else if (document.getElementById("animalslevel3show").style.display == "block") {
                timer = setTimeout(animalslevel3question, 3000);
                document.getElementById("keepgoing").style.display = "block";
                document.getElementById("animalslevel3show").style.display = "none";
                document.getElementById("animalslevel3choices").style.display = "none";
            }
            else if (document.getElementById("animalslevel4show").style.display == "block") {
                timer = setTimeout(animalslevel4question, 3000);
                document.getElementById("keepgoing").style.display = "block";
                document.getElementById("animalslevel4show").style.display = "none";
                document.getElementById("animalslevel4choices").style.display = "none";
            }
            else if (document.getElementById("animalslevel5show").style.display == "block") {
                timer = setTimeout(animalslevel5question, 3000);
                document.getElementById("keepgoing").style.display = "block";
                document.getElementById("animalslevel5show").style.display = "none";
                document.getElementById("animalslevel5choices").style.display = "none";
            }
            else if (document.getElementById("colorslevel1show").style.display == "block") {
                timer = setTimeout(colorslevel1question, 3000);
                document.getElementById("keepgoing").style.display = "block";
                document.getElementById("colorslevel1show").style.display = "none";
                document.getElementById("colorslevel1choices").style.display = "none";
            }
            else if (document.getElementById("colorslevel2show").style.display == "block") {
                timer = setTimeout(colorslevel2question, 3000);
                document.getElementById("keepgoing").style.display = "block";
                document.getElementById("colorslevel2show").style.display = "none";
                document.getElementById("colorslevel2choices").style.display = "none";
            }
            else if (document.getElementById("colorslevel3show").style.display == "block") {
                timer = setTimeout(colorslevel3question, 3000);
                document.getElementById("keepgoing").style.display = "block";
                document.getElementById("colorslevel3show").style.display = "none";
                document.getElementById("colorslevel3choices").style.display = "none";
            }
            else if (document.getElementById("colorslevel4show").style.display == "block") {
                timer = setTimeout(colorslevel4question, 3000);
                document.getElementById("keepgoing").style.display = "block";
                document.getElementById("colorslevel4show").style.display = "none";
                document.getElementById("colorslevel4choices").style.display = "none";
            }
            else if (document.getElementById("colorslevel5show").style.display == "block") {
                timer = setTimeout(colorslevel5question, 3000);
                document.getElementById("keepgoing").style.display = "block";
                document.getElementById("colorslevel5show").style.display = "none";
                document.getElementById("colorslevel5choices").style.display = "none";
            }
            else if (document.getElementById("foodslevel1show").style.display == "block") {
                timer = setTimeout(foodslevel1question, 3000);
                document.getElementById("keepgoing").style.display = "block";
                document.getElementById("foodslevel1show").style.display = "none";
                document.getElementById("foodslevel1choices").style.display = "none";
            }
            else if (document.getElementById("foodslevel2show").style.display == "block") {
                timer = setTimeout(foodslevel2question, 3000);
                document.getElementById("keepgoing").style.display = "block";
                document.getElementById("foodslevel2show").style.display = "none";
                document.getElementById("foodslevel2choices").style.display = "none";
            }
            else if (document.getElementById("foodslevel3show").style.display == "block") {
                timer = setTimeout(foodslevel3question, 3000);
                document.getElementById("keepgoing").style.display = "block";
                document.getElementById("foodslevel3show").style.display = "none";
                document.getElementById("foodslevel3choices").style.display = "none";
            }
            else if (document.getElementById("foodslevel4show").style.display == "block") {
                timer = setTimeout(foodslevel4question, 3000);
                document.getElementById("keepgoing").style.display = "block";
                document.getElementById("foodslevel4show").style.display = "none";
                document.getElementById("foodslevel4choices").style.display = "none";
            }
            else if (document.getElementById("foodslevel5show").style.display == "block") {
                timer = setTimeout(foodslevel5question, 3000);
                document.getElementById("keepgoing").style.display = "block";
                document.getElementById("foodslevel5show").style.display = "none";
                document.getElementById("foodslevel5choices").style.display = "none";
            }
            break;
        case 'gm':
            if (document.getElementById("numberslevel1show").style.display == "block") {
                timer = setTimeout(numberslevel2, 3000);
                playAudio();
                document.getElementById("goldmedal").style.display = "block";
                document.getElementById("numberslevel1show").style.display = "none";
                document.getElementById("numberslevel1choices").style.display = "none";
            }
            else if (document.getElementById("numberslevel2show").style.display == "block") {
                playAudio();
                timer = setTimeout(numberslevel3, 3000);
                document.getElementById("goldmedal").style.display = "block";
                document.getElementById("numberslevel2show").style.display = "none";
                document.getElementById("numberslevel2choices").style.display = "none";
            }
            else if (document.getElementById("numberslevel3show").style.display == "block") {
                playAudio();
                timer = setTimeout(numberslevel4, 3000);
                document.getElementById("goldmedal").style.display = "block";
                document.getElementById("numberslevel3show").style.display = "none";
                document.getElementById("numberslevel3choices").style.display = "none";
            }
            else if (document.getElementById("numberslevel4show").style.display == "block") {
                playAudio();
                timer = setTimeout(numberslevel5, 3000);
                document.getElementById("goldmedal").style.display = "block";
                document.getElementById("numberslevel4show").style.display = "none";
                document.getElementById("numberslevel4choices").style.display = "none";
            }
            else if (document.getElementById("numberslevel5show").style.display == "block") {
                playAudio();
                timer = setTimeout(cong, 3000);
                document.getElementById("goldmedal").style.display = "block";
                document.getElementById("numberslevel5show").style.display = "none";
                document.getElementById("numberslevel5choices").style.display = "none";
            }
            else if (document.getElementById("alphabetlevel1show").style.display == "block") {
                playAudio();
                timer = setTimeout(alphabetlevel2, 3000);
                document.getElementById("goldmedal").style.display = "block";
                document.getElementById("alphabetlevel1show").style.display = "none";
                document.getElementById("alphabetlevel1choices").style.display = "none";
            }
            else if (document.getElementById("alphabetlevel2show").style.display == "block") {
                playAudio();
                timer = setTimeout(alphabetlevel3, 3000);
                document.getElementById("goldmedal").style.display = "block";
                document.getElementById("alphabetlevel2show").style.display = "none";
                document.getElementById("alphabetlevel2choices").style.display = "none";
            }
            else if (document.getElementById("alphabetlevel3show").style.display == "block") {
                playAudio();
                timer = setTimeout(alphabetlevel4, 3000);
                document.getElementById("goldmedal").style.display = "block";
                document.getElementById("alphabetlevel3show").style.display = "none";
                document.getElementById("alphabetlevel3choices").style.display = "none";
            }
            else if (document.getElementById("alphabetlevel4show").style.display == "block") {
                playAudio();
                timer = setTimeout(alphabetlevel5, 3000);
                document.getElementById("goldmedal").style.display = "block";
                document.getElementById("alphabetlevel4show").style.display = "none";
                document.getElementById("alphabetlevel4choices").style.display = "none";
            }
            else if (document.getElementById("alphabetlevel5show").style.display == "block") {
                playAudio();
                timer = setTimeout(cong, 3000);
                document.getElementById("goldmedal").style.display = "block";
                document.getElementById("alphabetlevel5show").style.display = "none";
                document.getElementById("alphabetlevel5choices").style.display = "none";
            }
            else if (document.getElementById("animalslevel1show").style.display == "block") {
                playAudio();
                timer = setTimeout(animalslevel2, 3000);
                document.getElementById("goldmedal").style.display = "block";
                document.getElementById("animalslevel1show").style.display = "none";
                document.getElementById("animalslevel1choices").style.display = "none";
            }
            else if (document.getElementById("animalslevel2show").style.display == "block") {
                playAudio();
                timer = setTimeout(animalslevel3, 3000);
                document.getElementById("goldmedal").style.display = "block";
                document.getElementById("animalslevel2show").style.display = "none";
                document.getElementById("animalslevel2choices").style.display = "none";
            }
            else if (document.getElementById("animalslevel3show").style.display == "block") {
                playAudio();
                timer = setTimeout(animalslevel4, 3000);
                document.getElementById("goldmedal").style.display = "block";
                document.getElementById("animalslevel3show").style.display = "none";
                document.getElementById("animalslevel3choices").style.display = "none";
            }
            else if (document.getElementById("animalslevel4show").style.display == "block") {
                playAudio();
                timer = setTimeout(animalslevel5, 3000);
                document.getElementById("goldmedal").style.display = "block";
                document.getElementById("animalslevel4show").style.display = "none";
                document.getElementById("animalslevel4choices").style.display = "none";
            }
            else if (document.getElementById("animalslevel5show").style.display == "block") {
                playAudio();
                timer = setTimeout(cong, 3000);
                document.getElementById("goldmedal").style.display = "block";
                document.getElementById("animalslevel5show").style.display = "none";
                document.getElementById("animalslevel5choices").style.display = "none";
            }
            else if (document.getElementById("colorslevel1show").style.display == "block") {
                playAudio();
                timer = setTimeout(colorslevel2, 3000);
                document.getElementById("goldmedal").style.display = "block";
                document.getElementById("colorslevel1show").style.display = "none";
                document.getElementById("colorslevel1choices").style.display = "none";
            }
            else if (document.getElementById("colorslevel2show").style.display == "block") {
                playAudio();
                timer = setTimeout(colorslevel3, 3000);
                document.getElementById("goldmedal").style.display = "block";
                document.getElementById("colorslevel2show").style.display = "none";
                document.getElementById("colorslevel2choices").style.display = "none";
            }
            else if (document.getElementById("colorslevel3show").style.display == "block") {
                playAudio();
                timer = setTimeout(colorslevel4, 3000);
                document.getElementById("goldmedal").style.display = "block";
                document.getElementById("colorslevel3show").style.display = "none";
                document.getElementById("colorslevel3choices").style.display = "none";
            }
            else if (document.getElementById("colorslevel4show").style.display == "block") {
                playAudio();
                timer = setTimeout(colorslevel5, 3000);
                document.getElementById("goldmedal").style.display = "block";
                document.getElementById("colorslevel4show").style.display = "none";
                document.getElementById("colorslevel4choices").style.display = "none";
            }
            else if (document.getElementById("colorslevel5show").style.display == "block") {
                playAudio();
                timer = setTimeout(cong, 3000);
                document.getElementById("goldmedal").style.display = "block";
                document.getElementById("colorslevel5show").style.display = "none";
                document.getElementById("colorslevel5choices").style.display = "none";
            }
            else if (document.getElementById("foodslevel1show").style.display == "block") {
                playAudio();
                timer = setTimeout(foodslevel2, 3000);
                document.getElementById("goldmedal").style.display = "block";
                document.getElementById("foodslevel1show").style.display = "none";
                document.getElementById("foodslevel1choices").style.display = "none";
            }
            else if (document.getElementById("foodslevel2show").style.display == "block") {
                playAudio();
                timer = setTimeout(foodslevel3, 3000);
                document.getElementById("goldmedal").style.display = "block";
                document.getElementById("foodslevel2show").style.display = "none";
                document.getElementById("foodslevel2choices").style.display = "none";
            }
            else if (document.getElementById("foodslevel3show").style.display == "block") {
                playAudio();
                timer = setTimeout(foodslevel4, 3000);
                document.getElementById("goldmedal").style.display = "block";
                document.getElementById("foodslevel3show").style.display = "none";
                document.getElementById("foodslevel3choices").style.display = "none";
            }
            else if (document.getElementById("foodslevel4show").style.display == "block") {
                playAudio();
                timer = setTimeout(foodslevel5, 3000);
                document.getElementById("goldmedal").style.display = "block";
                document.getElementById("foodslevel4show").style.display = "none";
                document.getElementById("foodslevel4choices").style.display = "none";
            }
            else if (document.getElementById("foodslevel5show").style.display == "block") {
                playAudio();
                timer = setTimeout(cong, 3000);
                document.getElementById("goldmedal").style.display = "block";
                document.getElementById("foodslevel5show").style.display = "none";
                document.getElementById("foodslevel5choices").style.display = "none";
            }
                break;
      }
}

function fadetoblack() {
    if (document.getElementById("numberslevel1").style.display == "block") {
        timer = setTimeout(numberslevel1question, 3000);
        document.getElementById("fade").style.display = "block";
        document.getElementById("numberslevel1").style.display = "none";
    }
    else if (document.getElementById("numberslevel2").style.display == "block") {
        timer = setTimeout(numberslevel2question, 3000);
        document.getElementById("fade").style.display = "block";
        document.getElementById("numberslevel2").style.display = "none";
    }
    else if (document.getElementById("numberslevel3").style.display == "block") {
        timer = setTimeout(numberslevel3question, 3000);
        document.getElementById("fade").style.display = "block";
        document.getElementById("numberslevel3").style.display = "none";
    }
    else if (document.getElementById("numberslevel4").style.display == "block") {
        timer = setTimeout(numberslevel4question, 3000);
        document.getElementById("fade").style.display = "block";
        document.getElementById("numberslevel4").style.display = "none";
    }
    else if (document.getElementById("numberslevel5").style.display == "block") {
        timer = setTimeout(numberslevel5question, 3000);
        document.getElementById("fade").style.display = "block";
        document.getElementById("numberslevel5").style.display = "none";
    }
    else if (document.getElementById("alphabetlevel1").style.display == "block") {
        timer = setTimeout(alphabetlevel1question, 3000);
        document.getElementById("fade").style.display = "block";
        document.getElementById("alphabetlevel1").style.display = "none";
    }
    else if (document.getElementById("alphabetlevel2").style.display == "block") {
        timer = setTimeout(alphabetlevel2question, 3000);
        document.getElementById("fade").style.display = "block";
        document.getElementById("alphabetlevel2").style.display = "none";
    }
    else if (document.getElementById("alphabetlevel3").style.display == "block") {
        timer = setTimeout(alphabetlevel3question, 3000);
        document.getElementById("fade").style.display = "block";
        document.getElementById("alphabetlevel3").style.display = "none";
    }
    else if (document.getElementById("alphabetlevel4").style.display == "block") {
        timer = setTimeout(alphabetlevel4question, 3000);
        document.getElementById("fade").style.display = "block";
        document.getElementById("alphabetlevel4").style.display = "none";
    }
    else if (document.getElementById("alphabetlevel5").style.display == "block") {
        timer = setTimeout(alphabetlevel5question, 3000);
        document.getElementById("fade").style.display = "block";
        document.getElementById("alphabetlevel5").style.display = "none";
    }
    else if (document.getElementById("animalslevel1").style.display == "block") {
        timer = setTimeout(animalslevel1question, 3000);
        document.getElementById("fade").style.display = "block";
        document.getElementById("animalslevel1").style.display = "none";
    }
    else if (document.getElementById("animalslevel2").style.display == "block") {
        timer = setTimeout(animalslevel2question, 3000);
        document.getElementById("fade").style.display = "block";
        document.getElementById("animalslevel2").style.display = "none";
    }
    else if (document.getElementById("animalslevel3").style.display == "block") {
        timer = setTimeout(animalslevel3question, 3000);
        document.getElementById("fade").style.display = "block";
        document.getElementById("animalslevel3").style.display = "none";
    }
    else if (document.getElementById("animalslevel4").style.display == "block") {
        timer = setTimeout(animalslevel4question, 3000);
        document.getElementById("fade").style.display = "block";
        document.getElementById("animalslevel4").style.display = "none";
    }
    else if (document.getElementById("animalslevel5").style.display == "block") {
        timer = setTimeout(animalslevel5question, 3000);
        document.getElementById("fade").style.display = "block";
        document.getElementById("animalslevel5").style.display = "none";
    }
    else if (document.getElementById("colorslevel1").style.display == "block") {
        timer = setTimeout(colorslevel1question, 3000);
        document.getElementById("fade").style.display = "block";
        document.getElementById("colorslevel1").style.display = "none";
    }
    else if (document.getElementById("colorslevel2").style.display == "block") {
        timer = setTimeout(colorslevel2question, 3000);
        document.getElementById("fade").style.display = "block";
        document.getElementById("colorslevel2").style.display = "none";
    }
    else if (document.getElementById("colorslevel3").style.display == "block") {
        timer = setTimeout(colorslevel3question, 3000);
        document.getElementById("fade").style.display = "block";
        document.getElementById("colorslevel3").style.display = "none";
    }
    else if (document.getElementById("colorslevel4").style.display == "block") {
        timer = setTimeout(colorslevel4question, 3000);
        document.getElementById("fade").style.display = "block";
        document.getElementById("colorslevel4").style.display = "none";
    }
    else if (document.getElementById("colorslevel5").style.display == "block") {
        timer = setTimeout(colorslevel5question, 3000);
        document.getElementById("fade").style.display = "block";
        document.getElementById("colorslevel5").style.display = "none";
    }
    else if (document.getElementById("foodslevel1").style.display == "block") {
        timer = setTimeout(foodslevel1question, 3000);
        document.getElementById("fade").style.display = "block";
        document.getElementById("foodslevel1").style.display = "none";
    }
    else if (document.getElementById("foodslevel2").style.display == "block") {
        timer = setTimeout(foodslevel2question, 3000);
        document.getElementById("fade").style.display = "block";
        document.getElementById("foodslevel2").style.display = "none";
    }
    else if (document.getElementById("foodslevel3").style.display == "block") {
        timer = setTimeout(foodslevel3question, 3000);
        document.getElementById("fade").style.display = "block";
        document.getElementById("foodslevel3").style.display = "none";
    }
    else if (document.getElementById("foodslevel4").style.display == "block") {
        timer = setTimeout(foodslevel4question, 3000);
        document.getElementById("fade").style.display = "block";
        document.getElementById("foodslevel4").style.display = "none";
    }
    else if (document.getElementById("foodslevel5").style.display == "block") {
        timer = setTimeout(foodslevel5question, 3000);
        document.getElementById("fade").style.display = "block";
        document.getElementById("foodslevel5").style.display = "none";
    }
}

        function numberslevel1question() {
            document.getElementById("fade").style.display = "none";
            document.getElementById("numberslevel1show").style.display = "block";
            document.getElementById("numberslevel1choices").style.display = "block";
            document.getElementById("keepgoing").style.display = "none";
        }

        function numberslevel2() {
            timer = setTimeout(fadetoblack, 3000);
            document.getElementById("numberslevel2").style.display = "block";
            document.getElementById("goldmedal").style.display = "none";
        }

        function numberslevel2question() {
            document.getElementById("fade").style.display = "none";
            document.getElementById("numberslevel2show").style.display = "block";
            document.getElementById("numberslevel2choices").style.display = "block";
            document.getElementById("keepgoing").style.display = "none";
        }

        function numberslevel3() {
            timer = setTimeout(fadetoblack, 5000);
            document.getElementById("numberslevel3").style.display = "block";
            document.getElementById("goldmedal").style.display = "none";
        }

        function numberslevel3question() {
            document.getElementById("fade").style.display = "none";
            document.getElementById("numberslevel3show").style.display = "block";
            document.getElementById("numberslevel3choices").style.display = "block";
            document.getElementById("keepgoing").style.display = "none";
        }

        function numberslevel4() {
            timer = setTimeout(fadetoblack, 6000);
            document.getElementById("numberslevel4").style.display = "block";
            document.getElementById("goldmedal").style.display = "none";
        }

        function numberslevel4question() {
            document.getElementById("fade").style.display = "none";
            document.getElementById("numberslevel4show").style.display = "block";
            document.getElementById("numberslevel4choices").style.display = "block";
            document.getElementById("keepgoing").style.display = "none";
        }

        function numberslevel5() {
            timer = setTimeout(fadetoblack, 7000);
            document.getElementById("numberslevel5").style.display = "block";
            document.getElementById("goldmedal").style.display = "none";
        }

        function numberslevel5question() {
            document.getElementById("fade").style.display = "none";
            document.getElementById("numberslevel5show").style.display = "block";
            document.getElementById("numberslevel5choices").style.display = "block";
            document.getElementById("keepgoing").style.display = "none";
        }

        function cong() {
            document.getElementById("congrats").style.display = "block";
            document.getElementById("goldmedal").style.display = "none";
        }

        function alphabetlevel1question() {
            document.getElementById("fade").style.display = "none";
            document.getElementById("alphabetlevel1show").style.display = "block";
            document.getElementById("alphabetlevel1choices").style.display = "block";
            document.getElementById("keepgoing").style.display = "none";
        }

        function alphabetlevel2() {
            timer = setTimeout(fadetoblack, 3000);
            document.getElementById("alphabetlevel2").style.display = "block";
            document.getElementById("goldmedal").style.display = "none";
        }

        function alphabetlevel2question() {
            document.getElementById("fade").style.display = "none";
            document.getElementById("alphabetlevel2show").style.display = "block";
            document.getElementById("alphabetlevel2choices").style.display = "block";
            document.getElementById("keepgoing").style.display = "none";
        }

        function alphabetlevel3() {
            timer = setTimeout(fadetoblack, 5000);
            document.getElementById("alphabetlevel3").style.display = "block";
            document.getElementById("goldmedal").style.display = "none";
        }

        function alphabetlevel3question() {
            document.getElementById("fade").style.display = "none";
            document.getElementById("alphabetlevel3show").style.display = "block";
            document.getElementById("alphabetlevel3choices").style.display = "block";
            document.getElementById("keepgoing").style.display = "none";
        }

        function alphabetlevel4() {
            timer = setTimeout(fadetoblack, 6000);
            document.getElementById("alphabetlevel4").style.display = "block";
            document.getElementById("goldmedal").style.display = "none";
        }

        function alphabetlevel4question() {
            document.getElementById("fade").style.display = "none";
            document.getElementById("alphabetlevel4show").style.display = "block";
            document.getElementById("alphabetlevel4choices").style.display = "block";
            document.getElementById("keepgoing").style.display = "none";
        }

        function alphabetlevel5() {
            timer = setTimeout(fadetoblack, 7000);
            document.getElementById("alphabetlevel5").style.display = "block";
            document.getElementById("goldmedal").style.display = "none";
        }

        function alphabetlevel5question() {
            document.getElementById("fade").style.display = "none";
            document.getElementById("alphabetlevel5show").style.display = "block";
            document.getElementById("alphabetlevel5choices").style.display = "block";
            document.getElementById("keepgoing").style.display = "none";
        }

        function animalslevel1question() {
            document.getElementById("fade").style.display = "none";
            document.getElementById("animalslevel1show").style.display = "block";
            document.getElementById("animalslevel1choices").style.display = "block";
            document.getElementById("keepgoing").style.display = "none";
        }

        function animalslevel2() {
            timer = setTimeout(fadetoblack, 3000);
            document.getElementById("animalslevel2").style.display = "block";
            document.getElementById("goldmedal").style.display = "none";
        }

        function animalslevel2question() {
            document.getElementById("fade").style.display = "none";
            document.getElementById("animalslevel2show").style.display = "block";
            document.getElementById("animalslevel2choices").style.display = "block";
            document.getElementById("keepgoing").style.display = "none";
        }

        function animalslevel3() {
            timer = setTimeout(fadetoblack, 5000);
            document.getElementById("animalslevel3").style.display = "block";
            document.getElementById("goldmedal").style.display = "none";
        }

        function animalslevel3question() {
            document.getElementById("fade").style.display = "none";
            document.getElementById("animalslevel3show").style.display = "block";
            document.getElementById("animalslevel3choices").style.display = "block";
            document.getElementById("keepgoing").style.display = "none";
        }

        function animalslevel4() {
            timer = setTimeout(fadetoblack, 6000);
            document.getElementById("animalslevel4").style.display = "block";
            document.getElementById("goldmedal").style.display = "none";
        }

        function animalslevel4question() {
            document.getElementById("fade").style.display = "none";
            document.getElementById("animalslevel4show").style.display = "block";
            document.getElementById("animalslevel4choices").style.display = "block";
            document.getElementById("keepgoing").style.display = "none";
        }

        function animalslevel5() {
            timer = setTimeout(fadetoblack, 7000);
            document.getElementById("animalslevel5").style.display = "block";
            document.getElementById("goldmedal").style.display = "none";
        }

        function animalslevel5question() {
            document.getElementById("fade").style.display = "none";
            document.getElementById("animalslevel5show").style.display = "block";
            document.getElementById("animalslevel5choices").style.display = "block";
            document.getElementById("keepgoing").style.display = "none";
        }

        function colorslevel1question() {
            document.getElementById("fade").style.display = "none";
            document.getElementById("colorslevel1show").style.display = "block";
            document.getElementById("colorslevel1choices").style.display = "block";
            document.getElementById("keepgoing").style.display = "none";
        }

        function colorslevel2() {
            timer = setTimeout(fadetoblack, 3000);
            document.getElementById("colorslevel2").style.display = "block";
            document.getElementById("goldmedal").style.display = "none";
        }

        function colorslevel2question() {
            document.getElementById("fade").style.display = "none";
            document.getElementById("colorslevel2show").style.display = "block";
            document.getElementById("colorslevel2choices").style.display = "block";
            document.getElementById("keepgoing").style.display = "none";
        }

        function colorslevel3() {
            timer = setTimeout(fadetoblack, 5000);
            document.getElementById("colorslevel3").style.display = "block";
            document.getElementById("goldmedal").style.display = "none";
        }

        function colorslevel3question() {
            document.getElementById("fade").style.display = "none";
            document.getElementById("colorslevel3show").style.display = "block";
            document.getElementById("colorslevel3choices").style.display = "block";
            document.getElementById("keepgoing").style.display = "none";
        }

        function colorslevel4() {
            timer = setTimeout(fadetoblack, 6000);
            document.getElementById("colorslevel4").style.display = "block";
            document.getElementById("goldmedal").style.display = "none";
        }

        function colorslevel4question() {
            document.getElementById("fade").style.display = "none";
            document.getElementById("colorslevel4show").style.display = "block";
            document.getElementById("colorslevel4choices").style.display = "block";
            document.getElementById("keepgoing").style.display = "none";
        }

        function colorslevel5() {
            timer = setTimeout(fadetoblack, 7000);
            document.getElementById("colorslevel5").style.display = "block";
            document.getElementById("goldmedal").style.display = "none";
        }

        function colorslevel5question() {
            document.getElementById("fade").style.display = "none";
            document.getElementById("colorslevel5show").style.display = "block";
            document.getElementById("colorslevel5choices").style.display = "block";
            document.getElementById("keepgoing").style.display = "none";
        }

        function foodslevel1question() {
            document.getElementById("fade").style.display = "none";
            document.getElementById("foodslevel1show").style.display = "block";
            document.getElementById("foodslevel1choices").style.display = "block";
            document.getElementById("keepgoing").style.display = "none";
        }

        function foodslevel2() {
            timer = setTimeout(fadetoblack, 3000);
            document.getElementById("foodslevel2").style.display = "block";
            document.getElementById("goldmedal").style.display = "none";
        }

        function foodslevel2question() {
            document.getElementById("fade").style.display = "none";
            document.getElementById("foodslevel2show").style.display = "block";
            document.getElementById("foodslevel2choices").style.display = "block";
            document.getElementById("keepgoing").style.display = "none";
        }

        function foodslevel3() {
            timer = setTimeout(fadetoblack, 5000);
            document.getElementById("foodslevel3").style.display = "block";
            document.getElementById("goldmedal").style.display = "none";
        }

        function foodslevel3question() {
            document.getElementById("fade").style.display = "none";
            document.getElementById("foodslevel3show").style.display = "block";
            document.getElementById("foodslevel3choices").style.display = "block";
            document.getElementById("keepgoing").style.display = "none";
        }

        function foodslevel4() {
            timer = setTimeout(fadetoblack, 6000);
            document.getElementById("foodslevel4").style.display = "block";
            document.getElementById("goldmedal").style.display = "none";
        }

        function foodslevel4question() {
            document.getElementById("fade").style.display = "none";
            document.getElementById("foodslevel4show").style.display = "block";
            document.getElementById("foodslevel4choices").style.display = "block";
            document.getElementById("keepgoing").style.display = "none";
        }

        function foodslevel5() {
            timer = setTimeout(fadetoblack, 7000);
            document.getElementById("foodslevel5").style.display = "block";
            document.getElementById("goldmedal").style.display = "none";
        }

        function foodslevel5question() {
            document.getElementById("fade").style.display = "none";
            document.getElementById("foodslevel5show").style.display = "block";
            document.getElementById("foodslevel5choices").style.display = "block";
            document.getElementById("keepgoing").style.display = "none";
        }

        function showResults() {
            document.getElementById("alphabetresult").innerHTML = localStorage.alpcor || 0;
            document.getElementById("alphabetwrongresult").innerHTML = localStorage.alpwro || 0;
            document.getElementById("animalsresult").innerHTML = localStorage.anicor || 0;
            document.getElementById("animalswrongresult").innerHTML = localStorage.aniwro || 0;
            document.getElementById("colorsresult").innerHTML = localStorage.colcor || 0;
            document.getElementById("colorswrongresult").innerHTML = localStorage.colwro || 0;
            document.getElementById("foodsresult").innerHTML = localStorage.foocor || 0;
            document.getElementById("foodswrongresult").innerHTML = localStorage.foowro || 0;
            document.getElementById("numbersresult").innerHTML = localStorage.numcor || 0;
            document.getElementById("numberswrongresult").innerHTML = localStorage.numwro || 0;
        }

        function alphabetCorrect() {
            if (typeof (Storage) !== "undefined") {
                if (localStorage.alpcor) {
                    localStorage.alpcor = Number(localStorage.alpcor) + 1;
                }
                else {
                    localStorage.alpcor = 1;
                }
                document.getElementById("alphabetresult").innerHTML = localStorage.alpcor || 0;
            }
            else {
                document.getElementById("alphabetresult").innerHTML = "Sorry, it does not support web storage...";
            }
        }

        function alphabetWrong() {
            if (typeof (Storage) !== "undefined") {
                if (localStorage.alpwro) {
                    localStorage.alpwro = Number(localStorage.alpwro) + 1;
                }
                else {
                    localStorage.alpwro = 1;
                }
                document.getElementById("alphabetwrongresult").innerHTML = localStorage.alpwro || 0;
            }
            else {
                document.getElementById("alphabetwrongresult").innerHTML = "Sorry, it does not support web storage...";
            }
        }

        function animalsCorrect() {
            if (typeof (Storage) !== "undefined") {
                if (localStorage.anicor) {
                    localStorage.anicor = Number(localStorage.anicor) + 1;
                }
                else {
                    localStorage.anicor = 1;
                }
                document.getElementById("animalsresult").innerHTML = localStorage.anicor || 0;
            }
            else {
                document.getElementById("animalsresult").innerHTML = "Sorry, it does not support web storage...";
            }
        }

        function animalsWrong() {
            if (typeof (Storage) !== "undefined") {
                if (localStorage.aniwro) {
                    localStorage.aniwro = Number(localStorage.aniwro) + 1;
                }
                else {
                    localStorage.aniwro = 1;
                }
                document.getElementById("animalswrongresult").innerHTML = localStorage.aniwro || 0;
            }
            else {
                document.getElementById("animalswrongresult").innerHTML = "Sorry, it does not support web storage...";
            }
        }

        function colorsCorrect() {
            if (typeof (Storage) !== "undefined") {
                if (localStorage.colcor) {
                    localStorage.colcor = Number(localStorage.colcor) + 1;
                }
                else {
                    localStorage.colcor = 1;
                }
                document.getElementById("colorsresult").innerHTML = localStorage.colcor || 0;
            }
            else {
                document.getElementById("colorsresult").innerHTML = "Sorry, it does not support web storage...";
            }
        }

        function colorsWrong() {
            if (typeof (Storage) !== "undefined") {
                if (localStorage.colwro) {
                    localStorage.colwro = Number(localStorage.colwro) + 1;
                }
                else {
                    localStorage.colwro = 1;
                }
                document.getElementById("colorswrongresult").innerHTML = localStorage.colwro || 0;
            }
            else {
                document.getElementById("colorswrongresult").innerHTML = "Sorry, it does not support web storage...";
            }
        }

        function foodsCorrect() {
            if (typeof (Storage) !== "undefined") {
                if (localStorage.foocor) {
                    localStorage.foocor = Number(localStorage.foocor) + 1;
                }
                else {
                    localStorage.foocor = 1;
                }
                document.getElementById("foodsresult").innerHTML = localStorage.foocor || 0;
            }
            else {
                document.getElementById("foodsresult").innerHTML = "Sorry, it does not support web storage...";
            }
        }

        function foodsWrong() {
            if (typeof (Storage) !== "undefined") {
                if (localStorage.foowro) {
                    localStorage.foowro = Number(localStorage.foowro) + 1;
                }
                else {
                    localStorage.foowro = 1;
                }
                document.getElementById("foodswrongresult").innerHTML = localStorage.foowro || 0;
            }
            else {
                document.getElementById("foodswrongresult").innerHTML = "Sorry, it does not support web storage...";
            }
        }

        function numbersCorrect() {
            if (typeof (Storage) !== "undefined") {
                if (localStorage.numcor) {
                    localStorage.numcor = Number(localStorage.numcor) + 1;
                }
                else {
                    localStorage.numcor = 1;
                }
                document.getElementById("numbersresult").innerHTML = localStorage.numcor || 0;
            }
            else {
                document.getElementById("numbersresult").innerHTML = "Sorry, it does not support web storage...";
            }
        }

        function numbersWrong() {
            if (typeof (Storage) !== "undefined") {
                if (localStorage.numwro) {
                    localStorage.numwro = Number(localStorage.numwro) + 1;
                }
                else {
                    localStorage.numwro = 1;
                }
                document.getElementById("numberswrongresult").innerHTML = localStorage.numwro || 0;
            }
            else {
                document.getElementById("numberswrongresult").innerHTML = "Sorry, it does not support web storage...";
            }
        }


(function() {
  var resetButton = document.querySelector('.reset');
  resetButton.addEventListener('click', function () {
    localStorage.setItem('alpcor', '0');
    localStorage.setItem('alpwro', '0');
    localStorage.setItem('colcor', '0');
    localStorage.setItem('colwro', '0');
    localStorage.setItem('numwro', '0');
    localStorage.setItem('numcor', '0');
    localStorage.setItem('anicor', '0');
    localStorage.setItem('aniwro', '0');
    localStorage.setItem('foocor', '0');
    localStorage.setItem('foowro', '0');
    showResults();
  });

  var sendMailButton = document.querySelector('.send-results');
  sendMailButton.addEventListener('click', function () {
    var param = {
      message: $('.results-table').prop('outerHTML'),
      to: $('.emailTo').val()
    };

    $.post('http://piccom.herokuapp.com/mail', param, function (data) {
      alert('Email Sent');
    });
  });
}());
